import Tabs from './components/Tabs/v1/Tabs'
import Tab from './components/Tabs/v1/Tab'
import ErrorBanner from './components/ErrorBanner/ErrorBanner'

export default {
  Tabs,
  Tab,
  ErrorBanner,
}
